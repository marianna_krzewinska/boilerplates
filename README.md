# Boilerplates

This is meant to be a repository of boilerplates for development kickstart with:
* project structure
* various configurations and boilerplates
* all using a command line tool

---
### Available boilerplates
1. [NodeJS express server](./server-node-js/README.md)
   * Pug template engine
   * Morgan & Winston logs configured
   * Jest test framework
   * Helmet & CORS (security headers)
2. [TypeScript express server](./server-node-ts/README.md)
   * Pug template engine
   * Morgan & Winston logs configured
   * Jest test framework
   * Helmet & CORS (security headers)
3. [React SPA with TypeScript](./react-spa/README.md)
   * Based on `create-react-app`
   * Added Dockerfile, Dockerfile.dev and docker-compose.yaml configuration
   * Helm chart serving build files
4. [React SPA with Redux](./react-spa-redux/README.md)
   * Based on `create-react-app` and `React SPA with TypeScript`
   * Added Redux setup and example
   * Helm chart serving build files
5. [Flutter](./flutter/README.md)
   * Dockerized development with VS Code integration
   * Supports Mobile phone development
   * (todo) Android emulator
