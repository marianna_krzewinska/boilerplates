'use strict';

const joiOptions = {
  abortEarly: false, // include all errors
  allowUnknown: true,
  stripUnknown: true
};

const validateRequest = (req, next, schema) => {
  const { error, value } = schema.validate(req.body, joiOptions);

  if (error) {
    return next(`Validation error: ${error.details
      .map(x => x.message)
      .join(', ')}`);
  }

  req.body = value;
  return next();
}


module.exports = {
  validateRequest,
};
