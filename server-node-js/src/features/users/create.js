'use strict';

const Joi = require('joi');

const { validateRequest } = require('@src/utils/validator')


const schema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().email().required(),
});

const validateCreateUserReq = (req, res, next) => {
  validateRequest(req, next, schema);
};

const createUser = (req, res, next) => {
  res.send(req.body);
};


module.exports = {
  validateCreateUserReq,
  createUser,
}
