'use strict';

const express = require('express');

const {
  createUser,
  validateCreateUserReq,
} = require('./create');


const router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

/* POST create user */
router.post('/', validateCreateUserReq, createUser);


module.exports = router;
