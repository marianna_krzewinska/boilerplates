'use strict';

const express = require('express');

const usersRouter = require('./users/index');


const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

router.use('/users', usersRouter);


module.exports = router;
