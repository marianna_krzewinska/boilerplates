'use strict';

const morgan = require('morgan');
const winston = require('winston');


const logger = winston.createLogger({
  level: process.env.NODE_LOG_LEVEL || 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json(),
  ),
  defaultMeta: { service: 'api' },
  transports: [new winston.transports.Console()],
});

morgan.token('host', (req) => req.headers.host);

// Add User id to the log
morgan.token('user-id', (req, res) => '');

const jsonFormat = (tokens, req, res) => JSON.stringify({
  'host': tokens['host'](req) || '',
  'http-version': tokens['http-version'](req, res),
  'method': tokens['method'](req, res),
  'path': tokens['url'](req, res),
  'referrer': tokens['referrer'](req, res) || '',
  'res-content-length': tokens['res'](req, res, 'content-length') || 0,
  'status-code': tokens['status'](req, res),
  'total-time': tokens['total-time'](req, res),
  'user-agent': tokens['user-agent'](req, res),
  'user-id': tokens['user-id'](req, res),
});

const logMiddleware = morgan(jsonFormat, {
  stream: {
    write: message => logger.http('', JSON.parse(message)),
}});


module.exports = {
  logMiddleware,
  logger,
};
