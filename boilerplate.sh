#!/usr/bin/env bash

version=0.5.1
basePath=/tmp/boilerplate-generator
isDev=false
repoUrl=https://gitlab.com/marianna_krzewinska/boilerplates.git

options="
  (1) Node (JS)
  (2) Node (TS)
  (3) React SPA (TS)
  (4) React SPA with Redux (TS)
  (5) Flutter (v2)
"


# Handle help and version
case "$1" in
  "--help" | "-h")
    echo "
Boilerplate generator (v$version)

Usage: ./boilerplate.sh

  Starts an interactive session to add services and
  boilerplate of functionalities.

Available boilerplates:
$options

Other commmands:
    -h, --help          prints this help message
    -v, --version       prints version
    -d, --dev [PATH]    run in dev mode by passing a path to folder with boilerplate files

    "
    exit
    ;;
  "--version" | "-v")
    echo "$version"
    exit
    ;;
  "--dev" | "-d")
    if [ -z "$2" ]
    then
      echo "
  The PATH is required. See --help for details.
      "
      exit
    fi

    basePath=$2
    isDev=true
    ;;
esac


# Node JS config
nodeJSOption="node-js"
nodeJSPath=$basePath/server-node-js

# Node TS config
nodeTSOption="node-ts"
nodeTSPath=$basePath/server-node-ts

# React SPA config
reactSPATSOption="react-spa"
reactSPATSPath=$basePath/react-spa

# React SPA with Redux config
reactSPAReduxTSOption="react-spa-redux"
reactSPAReduxTSPath=$basePath/react-spa-redux

# Flutter config
flutterOption="flutter"
flutterPath=$basePath/flutter


# Clone repository with source files
if [ $isDev == "false" ]
then
  rm -rf $basePath
  git clone \
    --quiet \
    --depth 1 \
    --branch $version \
    $repoUrl \
    $basePath 2> /dev/null
fi


# Services to add
services=()

# Select and name services
while [ true ]; do
  echo "What service would you like to create?
$options
  "
  echo -n "Service type: "
  read serviceType

  echo -n "Service name: "
  read serviceName

  case "$serviceType" in
    "1")
      services+=("$nodeJSOption:$serviceName");
      ;;
    "2")
      services+=("$nodeTSOption:$serviceName");
      ;;
    "3")
      services+=("$reactSPATSOption:$serviceName");
      ;;
    "4")
      services+=("$reactSPAReduxTSOption:$serviceName");
      ;;
    "5")
      services+=("$flutterOption:$serviceName");
      ;;
    *)
      echo "Incorrect service type"
      ;;
  esac

  echo -n "One more? (y/N): "
  read addAnother

  if [ "$addAnother" != "y" ]; then break; fi
done


###
# createService serviceType serviceName
#
# Creates service folder with service files
##
createService() {
  if [ -d "$PWD/$2" ]
  then
    echo -n "Folder '$2' already exists. Recreate (y/N): "
    read recreateFolder

    if [ "$recreateFolder" != "y" ];
    then
      echo "Skipping setup of the '$2' service";

      return 0;
    fi
  fi

  case "$1" in
    "$nodeJSOption")
      rm -rf $PWD/$2

      echo "Creating Node JS app in $2..."
      rsync -aPq --exclude=docker-compose.yaml $nodeJSPath/ $PWD/$2
      echo "Created app '$2'!"
      ;;

    "$nodeTSOption")
      rm -rf $PWD/$2

      echo "Creating Node TS app in $2..."
      rsync -aPq --exclude=docker-compose.yaml $nodeTSPath/ $PWD/$2
      echo "Created app '$2'!"
      ;;

    "$reactSPATSOption")
      rm -rf $PWD/$2

      echo "Creating React app in $2..."
      rsync -aPq --exclude=docker-compose.yaml $reactSPATSPath/ $PWD/$2
      echo "Created app '$2'!"
      ;;

    "$reactSPAReduxTSOption")
      rm -rf $PWD/$2

      echo "Creating React app with Redux in $2..."
      rsync -aPq --exclude=docker-compose.yaml $reactSPAReduxTSPath/ $PWD/$2
      echo "Created app '$2'!"
      ;;

    "$flutterOption")
      rm -rf $PWD/$2

      echo "Creating Flutter app in $2..."
      rsync -aPq --exclude=docker-compose.yaml $flutterPath/ $PWD/$2
      echo "Created app '$2'!"
      ;;
  esac
}


###
# addToDockerCompose serviceType serviceName
#
# Creates docker-compose.yaml if doesn't exist;
# Adds service names by serviceName to docker-compose.yaml
##
addToDockerCompose() {
  # Create docker-compose.yaml file if it doesn't exist
  if [ ! -f "docker-compose.yaml" ]
  then
    cp $basePath/base-files/docker-compose.yaml $PWD/
  fi

  case "$1" in
    "$nodeJSOption")
      composePath=$nodeJSPath/docker-compose.yaml
      echo -n "Add service to docker-compose? (Y/n): "
      read addToDC

      if [ "$addToDC" != "n" ]
      then
        # Add service to docker-compose
        sed -i "/^services:/r $composePath" ./docker-compose.yaml
        sed -i "s/__FOLDER_NAME__/$2/g" ./docker-compose.yaml
        echo "Service '$2' added to docker-compose.yaml"
      else
        echo "Skipped adding service '$2' to docker-compose.yaml"
      fi
      ;;

    "$nodeTSOption")
      composePath=$nodeTSPath/docker-compose.yaml
      echo -n "Add service to docker-compose? (Y/n): "
      read addToDC

      if [ "$addToDC" != "n" ]
      then
        # Add service to docker-compose
        sed -i "/^services:/r $composePath" ./docker-compose.yaml
        sed -i "s/__FOLDER_NAME__/$2/g" ./docker-compose.yaml
        echo "Service '$2' added to docker-compose.yaml"
      else
        echo "Skipped adding service '$2' to docker-compose.yaml"
      fi
      ;;

    "$reactSPATSOption")
      composePath=$reactSPATSPath/docker-compose.yaml
      echo -n "Add service to docker-compose? (Y/n): "
      read addToDC

      if [ "$addToDC" != "n" ]
      then
        # Add service to docker-compose
        sed -i "/^services:/r $composePath" ./docker-compose.yaml
        sed -i "s/__FOLDER_NAME__/$2/g" ./docker-compose.yaml
        echo "Service '$2' added to docker-compose.yaml"
      else
        echo "Skipped adding service '$2' to docker-compose.yaml"
      fi
      ;;

    "$reactSPAReduxTSOption")
      composePath=$reactSPAReduxTSPath/docker-compose.yaml
      echo -n "Add service to docker-compose? (Y/n): "
      read addToDC

      if [ "$addToDC" != "n" ]
      then
        # Add service to docker-compose
        sed -i "/^services:/r $composePath" ./docker-compose.yaml
        sed -i "s/__FOLDER_NAME__/$2/g" ./docker-compose.yaml
        echo "Service '$2' added to docker-compose.yaml"
      else
        echo "Skipped adding service '$2' to docker-compose.yaml"
      fi
      ;;

    "$flutterOption")
      composePath=$flutterPath/docker-compose.yaml
      echo -n "Add service to docker-compose? (Y/n): "
      read addToDC

      if [ "$addToDC" != "n" ]
      then
        # Add service to docker-compose
        sed -i "/^services:/r $composePath" ./docker-compose.yaml
        sed -i "s/__FOLDER_NAME__/$2/g" ./docker-compose.yaml
        echo "Service '$2' added to docker-compose.yaml"
      else
        echo "Skipped adding service '$2' to docker-compose.yaml"
      fi
      ;;
  esac
}


# Create services
for service in "${services[@]}"; do
  serviceType="${service%:*}"
  serviceName="${service##*:}"

  # Create service folder
  createService $serviceType $serviceName

  # Docker Compose
  addToDockerCompose $serviceType $serviceName

  # Add Code Server shared config
  if [ ! -d "$PWD/.code-server" ]
  then
    rsync -aPq $basePath/base-files/.code-server $PWD
  fi
done


# Cleanup
if [ $isDev == "false" ]
then
  rm -rf $basePath
fi

echo "Success!"
