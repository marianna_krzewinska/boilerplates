FROM openjdk:8

ENV ANDROID_BUILD_TOOLS_VERSION=29.0.2 \
  ANDROID_VERSION=29 \
  ANDROID_SDK_TOOLS_URL=https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip \
  FLUTTER_VERSION=2.2.3 \
  CODE_SERVER_PORT=8000 \
  CODE_SERVER_VERSION=3.8.0

# Install Chrome Web Browser for Flutter web development
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list

# Prerequisites
RUN apt update && apt install -y \
  xz-utils \
  zip \
  libglu1-mesa \
  google-chrome-stable \
  && rm -rf /var/lib/apt/lists/*

# Code server
RUN wget https://github.com/cdr/code-server/releases/download/v${CODE_SERVER_VERSION}/code-server_${CODE_SERVER_VERSION}_amd64.deb
RUN dpkg -i ./code-server_${CODE_SERVER_VERSION}_amd64.deb
EXPOSE ${CODE_SERVER_PORT}

# Set up new user
RUN useradd -ms /bin/bash developer
USER developer
WORKDIR /home/developer

# Prepare Android directories and system variables
RUN mkdir -p Android/sdk
ENV ANDROID_SDK_ROOT /home/developer/Android/sdk
RUN mkdir -p .android && touch .android/repositories.cfg

# Set up Android SDK
RUN wget -O sdk-tools.zip ${ANDROID_SDK_TOOLS_URL}
RUN unzip sdk-tools.zip && rm sdk-tools.zip
RUN mv tools Android/sdk/tools
RUN cd Android/sdk/tools/bin && yes | ./sdkmanager --licenses
RUN cd Android/sdk/tools/bin &&  \
  ./sdkmanager "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
  "patcher;v4" "platform-tools" \
  "platforms;android-${ANDROID_VERSION}" "sources;android-${ANDROID_VERSION}"
ENV PATH "$PATH:/home/developer/Android/sdk/platform-tools"

# Download Flutter SDK
RUN git clone https://github.com/flutter/flutter.git -b ${FLUTTER_VERSION} --single-branch --depth 1
ENV PATH "$PATH:/home/developer/flutter/bin"
RUN flutter doctor
RUN flutter config --enable-web --no-analytics

WORKDIR /home/developer

RUN code-server \
  --install-extension dart-code.dart-code \
  --install-extension dart-code.flutter

CMD code-server --bind-addr 0.0.0.0:${CODE_SERVER_PORT} .
