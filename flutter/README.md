# Flutter in Docker

This project provides instructions and needed code for dockerized Flutter development. Below you'll find instructions on how to use it with Code Server and VS Code remote development.

Supported OS:
- Linux
- Windows & MacOS - not tested, but it's possible that the only OS specific point will be [USB mobile device connection](https://docs.sevenbridges.com/docs/mount-a-usb-drive-in-a-docker-container)

## Table of Contents

- [Flutter in Docker](#flutter-in-docker)
  - [Table of Contents](#table-of-contents)
  - [Requirements](#requirements)
  - [Usage](#usage)
    - [Create Flutter app](#create-flutter-app)
    - [Local development with Code Server](#local-development-with-code-server)
  - [VSC remote development](#vsc-remote-development)
    - [Additional requirements](#additional-requirements)
    - [Starting app](#starting-app)
  - [Other IDE development with Flutter run in docker container](#other-ide-development-with-flutter-run-in-docker-container)
    - [Testing connection with the phone](#testing-connection-with-the-phone)
    - [1. Running the app](#1-running-the-app)
    - [2. Flutter hot reload](#2-flutter-hot-reload)
  - [Common Issues](#common-issues)
    - [Error: Method not found: 'Vector3'. Error: 'Matrix4' isn't a type. Error: Getter not found: 'Matrix4'.](#error-method-not-found-vector3-error-matrix4-isnt-a-type-error-getter-not-found-matrix4)
    - [How to start Code Server on another port?](#how-to-start-code-server-on-another-port)
  - [Inspiration](#inspiration)

## Requirements

- Docker
- Docker Compose


## Usage

### Create Flutter app

In the folder with `docker-compose.yaml`:
```bash
[~/d/project] : docker-compose up

# Open Code Server in browser http://localhost:8000 & open console there

# Generate app files inside container
~$ flutter create [app name]
Creating project [app name]...
  app/android/.gitignore (created)
  ...

# exit container
```

Copy app name into `.vscode/launch.json` into fields: `name` and `cwd` in place of `myapp`

### Local development with Code Server

```bash
[~/d/flutter-docker] : docker-compose up --build
```

Code Server runs by default on port `8000` (see [running Code Server on another port](#id-like-to-start-codeserver-on-another-port)). There you can install Dart & Flutter extensions and start debug - press `F5` and select device on which to run the app. It can also ask you to select what type of debugging to run - in that case, select Dart and then your app name.

## VSC remote development

### Additional requirements

- Visual Studio Code

### Starting app

1. Install Remote Development, Dart and Flutter extensions.
2. Start remote development - run command: `Remote-Containers: Open Folder in Container` and select your project folder (top one, containing folder .devcontainer).
3. Once the VSCode opens a new window with Dev Container, press F5 and select device on which to run the app. It can also ask you to select what type of debugging to run - in that case, select Dart and then your app name.

## Other IDE development with Flutter run in docker container

This option requires more effort to run the application and there's no support for debugging

If there's an IDE that supports dockerized Flutter debugging, then please let me know - I'd like to make this a comprehensive dockerized Flutter development guide.

Create file `docker-compose.override.yml`

```yml
version: "3.3"
services:
  mobile:
    stdin_open: true
    working_dir: /home/developer/workspace/workspace/[app folder]
    command: sh -c "flutter pub get && flutter run"
```

### Testing connection with the phone

```bash
# Make sure to connect your phone and run:

[~/d/flutter-docker] : docker run -it --rm --privileged -v '/dev/bus/usb:/dev/bus/usb' flutter-docker_mobile flutter doctor

# You should see something like that if phone is correctly connected and all permissions granted:

Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel master, 1.26.0-2.0.pre.296, on Linux, locale en_US)
[✓] Android toolchain - develop for Android devices (Android SDK version 29.0.2)
[✗] Chrome - develop for the web (Cannot find Chrome executable at google-chrome)
    ! Cannot find Chrome. Try setting CHROME_EXECUTABLE to a Chrome executable.
[!] Android Studio (not installed)
[✓] Connected device (1 available)

! Doctor found issues in 2 categories.
```

### 1. Running the app

```bash
[~/d/flutter-docker] : docker-compose up --build

# The app should start on your phone in debug mode
```

### 2. Flutter hot reload

To hot reload you have to attach externally to the Flutter container

```bash
[~/d/flutter-docker] : docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED         STATUS         PORTS     NAMES
efc2fe73cf8c   flutter-docker_mobile   "sh -c 'rm -rf build…"   6 minutes ago   Up 6 minutes             flutter-docker_mobile_1

[~/d/flutter-docker] : docker attach flutter-docker_mobile_1
```

## Common Issues

### Error: Method not found: 'Vector3'. Error: 'Matrix4' isn't a type. Error: Getter not found: 'Matrix4'.

If you get an error containing any of these, there's a big chance that it's caused by lack of some libraries: `Error: Method not found: 'Vector3'. Error: 'Matrix4' isn't a type. Error: Getter not found: 'Matrix4'.`

* **Solution**: run `flutter pub get` in your Flutter app folder (e.g. workspace/workspace/myapp when running in Code Server) before starting the app. Flutter sometimes downloads them, and sometimes doesn't, so it's best to do it at least before the first run.

### How to start Code Server on another port?

Add ports override to `docker-compose.override.yml`. E.g. starting Code Server on port 9000.

```yml
version: "3.3"
services:
  mobile:
    ...
    ports:
      - "9000:8000"
    ...
```

## Inspiration

- [Blogpost about running Flutter app in VS Code](https://blog.codemagic.io/how-to-dockerize-flutter-apps/)
