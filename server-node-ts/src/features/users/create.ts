import Joi from 'joi';
import * as express from 'express';

import { validateRequest } from '@src/utils/validator';


const schema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().email().required(),
});

const validateCreateUserReq = (
  req: express.Request,
  res: express.Response,
  next) => {
  validateRequest(req, schema, next);
};

const createUser = (req: express.Request, res: express.Response) => {
  res.send(req.body);
};


export {
  validateCreateUserReq,
  createUser,
}
