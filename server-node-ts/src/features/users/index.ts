import express from 'express';

import {
  createUser,
  validateCreateUserReq,
} from './create';


const router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

/* POST create user */
router.post('/', validateCreateUserReq, createUser);


export { router }
