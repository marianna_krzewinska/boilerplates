import express from 'express';

import { router as usersRouter } from './users/index';


const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

router.use('/users', usersRouter);


export { router }
