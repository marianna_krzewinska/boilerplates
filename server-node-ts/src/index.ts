import cookieParser from 'cookie-parser';
import createError from 'http-errors';
import express from 'express';
import path from 'path';
import helmet from 'helmet';
import cors from 'cors';

import { httpLogMiddleware, logger } from '@src/logger';
import { router } from './features/index';


const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(httpLogMiddleware);

app.use(helmet()); // security headers
app.use(cors());
app.use(express.json({ limit: '1kb' }));
app.use(express.urlencoded({ extended: false, limit: '1kb' }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  logger.error(err);

  res.locals.message = err.message || err;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const port = process.env.API_PORT || 3000;

app.listen(port, () => logger.info(`Listening on port ${port}`));
