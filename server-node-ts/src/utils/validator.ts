import Joi from 'joi';
import * as express from 'express';


const joiOptions = {
  abortEarly: false, // include all errors
  allowUnknown: true,
  stripUnknown: true
};

const validateRequest = (req: express.Request, schema: Joi.Schema, next) => {
  const { error, value } = schema.validate(req.body, joiOptions);

  if (error) {
    return next(`Validation error: ${error.details
      .map(x => x.message)
      .join(', ')}`);
  }

  req.body = value;
  return next();
}


export { validateRequest }
