const sum = (a: number, b: number) => a + b;

describe('sum(a, b)', () => {
  test('should add two numbers', () => {
    expect(sum(1, 2)).toBe(3);
  });
});
